import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import data from './db';

// COMPONENTS
import Header from './components/header/header';
import News from './components/news/news'

class App extends Component {
    state = {
        news: data
    };

    render () {
        return (
            <div>
                <Header/>
                <News news={this.state.news}/>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.querySelector('#root'));
