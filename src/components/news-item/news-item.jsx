import React from 'react';

const NewsItem = ({ item }) => (
  <div className="news-item">
    <h3 className="news-item__title">{item.title}</h3>
    <p className="news-item__feed">{item.feed}</p>
  </div>
);

export default NewsItem;
