import React from 'react';
import NewsItem from '../news-item/news-item';

const News = ({ news }) => {
  const items = news.map(item => (
    <NewsItem key={item.id} item={item} />
  ));

  return (
    <div className="news">
      {items}
    </div>
  );
};

export default News;
