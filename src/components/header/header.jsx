import React, { Component } from 'react';
import './header.css';

class Header extends Component {
    inputChangeHandler = (event) => {
        this.setState({
            keyword: event.target.value
        })
    };

    render() {
        return (
            <header className="header" style={{background: 'red'}}>
                <div className="header__logo">Logo</div>
                <input type="text" onChange={ this.inputChangeHandler } className="header__input"/>
            </header>
        );
    }
}

export default Header;
